
from src.utils import PlotUtils
import numpy as np
import streamlit as st
from PIL import Image
from src.constants import INFERENCE_EXAMPLE, CM_PLOT_PATH
import matplotlib.pyplot as plt

def train_model(data) :
    df = PlotUtils.fetch_data(
        'http://localhost:5000/api/training',
        data,
        "Training model, please wait..."
    )
    col1, col2 = st.columns(2)
    
    col1.metric(label="Accuracy score", value=str(round(df["accuracy"], 4)))
    col2.metric(label="F1 score", value=str(round(df["f1"], 4)))
    
    plt.rcParams['figure.figsize'] = (6, 6)

    PlotUtils.plot_confusion_matrix(
        np.array(df["cm"]),
        classes=['Clear(0)', 'Fraudulent(1)'],
                    normalize=False,
        title='Current Model'
    )

    plot_path = str(CM_PLOT_PATH)
    plt.savefig(plot_path)
    plt.show()

    st.image(Image.open(CM_PLOT_PATH))

def get_inference(data) :
    res = PlotUtils.fetch_data(
        'http://localhost:5000/api/inference',
        data,
        'Running inference...'
    )
    msg = "Fraudulent" if int(res) else "Clear"
    st.success('Done!')
    st.metric(label="Status", value="Transaction: " + msg)