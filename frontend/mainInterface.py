import streamlit as st
from src.constants import INFERENCE_EXAMPLE
import json
import dash


def training_components() :
    name = st.text_input('Model name', placeholder='decisiontree')
    serialize = st.checkbox('Save model')
    train = st.button('Train Model')
    if train :
        dash.train_model({"serialize":serialize,
                "name":name
            })

def inference_components():
    indexs = [1, 2, 19]
    for i in indexs:
       INFERENCE_EXAMPLE[i] = st.slider('Transaction Feature ' + str(i),
                                     -10.0, 10.0, step=0.001, value=INFERENCE_EXAMPLE[i])
    INFERENCE_EXAMPLE[28] = st.number_input('Transaction Amount', value=1000, min_value=0, max_value=int(1e10), step=100)
    infer = st.button('Run Fraud Inference')
    if infer:
        dash.get_inference(INFERENCE_EXAMPLE)


def eda_components() :
    pass

components = {
    "EDA":eda_components,
    "Training" : training_components,
    "Inference": inference_components
}

st.title("Card Fraud Detection Dashboard")
st.sidebar.title("Data Themes")

sidebar_options = st.sidebar.selectbox(
    "Options",
    ("EDA", "Training", "Inference")
)
with open("frontend/options.json") as json_file:
    options = json.load(json_file)

st.header(options[sidebar_options]["header"])
st.info(options[sidebar_options]["info"])

components[sidebar_options]()



