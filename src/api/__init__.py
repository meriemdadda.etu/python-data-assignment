from flask import Flask

from src.api.index_routes import blueprint as index_blueprint
from src.api.inference_routes import blueprint as inference_blueprint
from src.api.training_routes import blueprint as training_blueprint
from src.api.analysis_routes import blueprint as analysis_blueprint


app = Flask(__name__)
app.register_blueprint(index_blueprint)
app.register_blueprint(inference_blueprint)
app.register_blueprint(training_blueprint)
app.register_blueprint(analysis_blueprint)

if __name__ == "__main__":
    app.run()
