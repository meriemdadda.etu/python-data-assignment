from flask import Blueprint, request
from src.models.aggregator_model import AggregatorModel
from src.training.train_pipeline import TrainingPipeline

blueprint = Blueprint('analysis', __name__, url_prefix='/api/analysis')


@blueprint.route('/')
@blueprint.route('/index')
def index():
    return "CARD FRAUD DETECTION API - ANALYSIS BLUEPRINT"


@blueprint.route('/', methods=['POST'])
def run_analysis():
    tp = TrainingPipeline()

    return {
        "corr" :tp.get_corr_matrix(),
    }
