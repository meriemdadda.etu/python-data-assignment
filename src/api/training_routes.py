from flask import Blueprint, request
from src.constants import AGGREGATOR_MODEL_PATH
from src.models.aggregator_model import AggregatorModel
import numpy as np
from src.training.train_pipeline import TrainingPipeline

model = AggregatorModel()
model.load(AGGREGATOR_MODEL_PATH)
blueprint = Blueprint('training', __name__, url_prefix='/api/training')


@blueprint.route('/')
@blueprint.route('/index')
def index():
    return "hello"



@blueprint.route('/', methods=['POST'])
def training():
    info = request.json
    tp = TrainingPipeline()
    tp.train(serialize=info["serialize"],
            model_name=info["name"]
        )
    cm = tp.get_confusion_matrix(plot_name=info["name"])
    accuracy, f1 = tp.get_model_perfomance()
    return {"accuracy":accuracy, "f1":f1, "cm":cm.tolist()}
