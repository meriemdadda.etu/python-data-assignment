from sklearn.svm import SVC

from src.models.base_model import BaseModel
from sklearn.linear_model import LogisticRegression


class SVCModel(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(
            model=LogisticRegression(**kwargs)
        )
